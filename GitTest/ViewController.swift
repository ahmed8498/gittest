//
//  ViewController.swift
//  GitTest
//
//  Created by Ahmed Mohamed on 01/10/2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.text = "Hello this is the second commit"
        welcomeLabel.text = "This is the new commit"
        // Do any additional setup after loading the view.
        
    }


}

